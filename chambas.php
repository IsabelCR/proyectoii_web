<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include 'inc/header_common.php'; ?>
	<title>Chambas</title>	
</head>
<body onload="set_user(), MYCHAMBAS.list()">
	
	<?php include 'inc/header.php'; ?>


	<div class="container">

		<div class="container_title">
			<div id="logo">
				<img src="imagen/chambas.png">
				<!--<p class="title" >Chambas</p>-->
			</div>
			<a href="#openModel"><input type="button" value="New" class="btn btn-primary" onClick="MYCHAMBAS.selector()"></a>
		</div>

		<div class="table">

			<table id = "content_table" border=1>
				

				
			</table>		
		</div>		
	</div>

	<div class="container_modal" id="openModel">			
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
				<form id="usrForm">
					<select id="client_chambas">						
					</select>
					<br/>
					<br/>
					<input type="text" name="Description" id="description_chambas" placeholder="Description">
					<br/>
					<br/>
					<input type="date" name="Date" id="date_chambas" placeholder="Date">
					<br/>
					<br/>
					<input type="text" name="Notes" id="notes_chambas" placeholder="Notes">
					<br/>
					<br/>
					<a href="chambas.html">Cancel</a>
			  		<input type="submit" value="Save" onclick="MYCHAMBAS.create()" class="button">
				</form>	
		</div>
	</div>
	
	<div class="container_modal" id="openModel_edit">			
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
				<form id="usrForm">
					<select id="client_chambas_edit">						
					</select>
					<br/>
					<br/>
					<input type="text" name="Description" id="description_chambas_edit" placeholder="Description">
					<br/>
					<br/>
					<input type="date" name="Date" id="date_chambas_edit" placeholder="Date">
					<br/>
					<br/>
					<input type="text" name="Notes" id="notes_chambas_edit" placeholder="Notes">
					<br/>
					<br/>
					<a href="chambas.html">Cancel</a>
			  		<input type="submit" value="Save" onclick="MYCHAMBAS.save_edit()" class="button">
				</form>	
		</div>
	</div>

<?php include 'inc/footer_common.php'; ?>

</body>
</html>
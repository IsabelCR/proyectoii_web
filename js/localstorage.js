var index;
var tbClients = localStorage.getItem('client');
	tbClients = JSON.parse(tbClients);
	if(tbClients == null) 
			tbClients = [];
var tbChambas = localStorage.getItem('chambas');
	tbChambas = JSON.parse(tbChambas);
	if(tbChambas == null) 
			tbChambas = [];
var tbInvoices = localStorage.getItem('invoice');
	tbInvoices = JSON.parse(tbInvoices);
	if(tbInvoices == null) 
			tbInvoices = []; 
var tbUsers = localStorage.getItem('user');
	tbUsers = JSON.parse(tbUsers);
	if(tbUsers == null) 
			tbUsers = [];


var username;
var CLOSE = window.location=document.getElementById('close_btn').href;


var MYCLIENT = MYCLIENT || {
	create: function() {
		if ($("#id").val()==""|| $("#firstName").val()==""|| $("#lastName").val()==""
			||$("#phone").val()=="" ) {
			alert("Please dont leave any blank spaces");
		}
		else{	
			var person={			
				firstName: document.getElementById('firstName').value,
				lastName: document.getElementById('lastName').value,
				phone: document.getElementById('phone').value,
				id: document.getElementById('id').value
			};

			if (localStorage.getItem('client')===null) {
				var arr_client=[person];
				localStorage.setItem('client', JSON.stringify(arr_client));
				this.list();	
			}else{		
				var str_client=localStorage.getItem('client');
				var arr_client=JSON.parse(str_client);
				arr_client.push(person);
				localStorage.setItem('client', JSON.stringify(arr_client));
				CLOSE;
			};
		}
	},
	edit: function(boton){ 
		index=boton;
		var cli = tbClients[index];
		$("#id_edit").val(cli.id);
		$("#fistName_edit").val(cli.firstName);
		$("#phone_edit").val(cli.phone);
		$("#lastName_edit").val(cli.lastName);		
	},	
	save_edit: function(){
		if ($("#id_edit").val()==""|| $("#fistName_edit").val()==""|| $("#lastName_edit").val()==""
			||$("#phone_edit").val()=="" ) {
			alert("Please dont leave any blank spaces");
			this.edit(index);
		}else {
		// var p=index;
		tbClients[index] = {
			id    : $("#id_edit").val(),
			firstName  : $("#fistName_edit").val(),
			phone : $("#phone_edit").val(),
			lastName : $("#lastName_edit").val()
		};
		localStorage.setItem('client', JSON.stringify(tbClients));
		CLOSE;
		}
	},

	list: function(){
		$("#content_table").html("");
		$("#content_table").html(
			"<thead>"+
			"	<tr id="+"tituloTablaCliente"+">"+
			"	<th>ID</th>"+
			"	<th>Full name</th>"+
			"	<th>Phone</th>"+
			"	<th>Action</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
		);
		for(var i in tbClients){
		var cli = tbClients[i];
	  	$("#content_table tbody").append("<tr>"+
									 "	<td>"+cli.id+"</td>" + 
									 "	<td>"+cli.firstName+" "+cli.lastName+"</td>" + 
									 "	<td>"+cli.phone+"</td>" + 
									 "	<td>  <a href="+"#openModel_edit"+"> <img "+" onClick="+"MYCLIENT.edit(this.alt)"+" src='imagen/edit.png' alt='"+i+"' class='btnEdit'/> </a>  <a href="+"#"+">  <img "+" onClick="+"MYCLIENT.delete(this.alt)"+" src='imagen/delete.png' alt='"+i+"' class='btnDelete'/> </a>  </td>" + 
	  								 "</tr>");
		}
	},
	delete: function(boton){
		var flag = confirm("Are you sure you want to delete this client?");
		if (flag) {
		index=boton;
		tbClients.splice(index, 1);
		localStorage.setItem("client", JSON.stringify(tbClients));
		alert("Client deleted.");
		this.list();
		CLOSE;
		}
	},	
};

var	 MYCHAMBAS = MYCHAMBAS || {
	create: function() {
		if ($("#client_chambas").val()==""|| $("#description_chambas").val()==""|| $("#date_chambas").val()==""
			||$("#notes_chambas").val()=="" ) {
			alert("Please dont leave any blank spaces");
		}
		else{	
			var chamba={			
				client: document.getElementById('client_chambas').value,
				description: document.getElementById('description_chambas').value,
				date: document.getElementById('date_chambas').value,
				notes: document.getElementById('notes_chambas').value
			};

			if (localStorage.getItem('chambas')===null) {
				var arr_chambas=[chamba]
				localStorage.setItem('chambas', JSON.stringify(arr_chambas));
				this.list();	
			}else{		
				var str_chambas=localStorage.getItem('chambas');
				var arr_chambas=JSON.parse(str_chambas);
				arr_chambas.push(chamba);
				localStorage.setItem('chambas', JSON.stringify(arr_chambas));
				CLOSE;
			};
		}
	},

	selector: function(){
		var option_selector = document.getElementById('client_chambas');
		for (i=0; i < (tbClients.length); i++){
			option_selector.options[i]= new Option(tbClients[i].firstName+" "+tbClients[i].lastName, tbClients[i].firstName+" "+tbClients[i].lastName);
		}
	},

	edit: function(boton){ 
		index=boton;
		var option_selector = document.getElementById('client_chambas_edit');
		for (i=0; i < (tbChambas.length); i++){
			option_selector.options[i]= new Option(tbChambas[i].client);
		}

		var chamba = tbChambas[index];
		$("#description_chambas_edit").val(chamba.description);
		$("#date_chambas_edit").val(chamba.date);
		$("#notes_chambas_edit").val(chamba.notes);
		document.getElementById('client_chambas_edit').selectedIndex = index;
	},

	save_edit: function(){
		if ($("#description_chambas_edit").val()==""|| $("#date_chambas_edit").val()==""|| $("#notes_chambas_edit").val()==""
			||$("#client_chambas_edit").val()=="" ) {
			alert("Please dont leave any blank spaces");
			this.edit(index);
		}else {
		// var p=index;
		tbChambas[index] = {
			client: document.getElementById('client_chambas_edit').value,
			description: document.getElementById('description_chambas_edit').value,
			date: document.getElementById('date_chambas_edit').value,
			notes: document.getElementById('notes_chambas_edit').value
		};
		localStorage.setItem('chambas', JSON.stringify(tbChambas));
		CLOSE;
		}
	},

	delete: function(boton){
		var flag = confirm("Are you sure you want to delete this chamba?");
		if (flag) {
		index=boton;
		tbChambas.splice(index, 1);
		localStorage.setItem('chambas', JSON.stringify(tbChambas));
		alert("Chamba deleted.");
		this.list();
		CLOSE;
		}
	},

	list: function(){
		$("#content_table").html("");
		$("#content_table").html(
			"<thead>"+
			"	<tr id="+"tituloTablaCliente"+">"+
			"	<th>Client</th>"+
			"	<th>Description</th>"+
			"	<th>Date</th>"+
			"   <th>Notes</th>"+
			"	<th>Action</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
		);
		for(var i in tbChambas){
		var chamba = tbChambas[i];
	  	$("#content_table tbody").append("<tr>"+
									 "	<td>"+chamba.client+"</td>" + 
									 "	<td>"+chamba.description+"</td>" + 
									 "	<td>"+chamba.date+"</td>" + 
									  "	<td>"+chamba.notes+"</td>" + 
									 "	<td>  <a href="+"#openModel_edit"+"> <img "+" onClick="+"MYCHAMBAS.edit(this.alt)"+" src='imagen/edit.png' alt='"+i+"' class='btnEdit'/> </a>  <a href="+"#"+">  <img "+" onClick="+"MYCHAMBAS.delete(this.alt)"+" src='imagen/delete.png' alt='"+i+"' class='btnDelete'/> </a>  </td>" + 
	  								 "</tr>");
		}
	},
};

var MYINVOICE = MYINVOICE || {
	create: function() {

		if ($("#client_invoice").val()==""|| $("#description").val()==""|| $("#date").val()==""
			||$("#amount").val()=="" ) {
			alert("Please dont leave any blank spaces");
		}
		else{	
			var invoice_a={			
				client: document.getElementById('client_invoice').value,
				date: document.getElementById('date').value,
				amount: document.getElementById('amount').value
			};

			if (localStorage.getItem('invoice')===null) {
				var arr_invoice=[invoice_a]
				localStorage.setItem('invoice', JSON.stringify(arr_invoice));
				this.list();	
			}else{		
				var str_invoice=localStorage.getItem('invoice');
				var arr_invoice=JSON.parse(str_invoice);
				arr_invoice.push(invoice_a);
				localStorage.setItem('invoice', JSON.stringify(arr_invoice));
				CLOSE;
			};
		}
	},

	selector: function(){
		var option_selector = document.getElementById('client_invoice');
		for (i=0; i < (tbChambas.length); i++){
			option_selector.options[i]= new Option(tbChambas[i].client+" - "+tbChambas[i].description);
		}
	   },

	edit: function(boton){
		index=boton;

		var option_selector = document.getElementById('client_chambas_edit');
		for (i=0; i < (tbInvoices.length); i++){
			option_selector.options[i]= new Option(tbInvoices[i].client);
		}

		var inv = tbInvoices[index];
		$("#date_edit").val(inv.date);
		$("#amount_edit").val(inv.amount);
		document.getElementById('client_chambas_edit').selectedIndex = index;		
	},

	save_edit: function(){

		if ($("#client_chambas_edit").val()==""|| $("#description_edit").val()==""|| $("#date_edit").val()==""
			||$("#amount_edit").val()=="" ){
			alert("Please dont leave any blank spaces");
			this.edit(index);
		}else {
		tbInvoices[index] = {
			client    : $("#client_chambas_edit").val(),
			date : $("#date_edit").val(),
			amount : $("#amount_edit").val()
		};

		localStorage.setItem('invoice', JSON.stringify(tbInvoices));
		this.list();
		CLOSE;
		}
	},

	list: function(){
		$("#content_table").html("");
		$("#content_table").html(
			"<thead>"+
			"	<tr id="+"tituloTablaCliente"+">"+
			"	<th>Client - Description</th>"+
			"	<th>Date</th>"+
			"	<th>Amount</th>"+
			"	<th>Action</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
		);

		for(var i in tbInvoices){
		var inv = tbInvoices[i];
	  	$("#content_table tbody").append("<tr>"+
									 "	<td>"+inv.client+"</td>" + 
									 "	<td>"+inv.date+"</td>" + 
									 "	<td>"+inv.amount+"</td>" + 
									 "	<td>  <a href="+"#openModel_edit"+"> <img "+" onClick="+"MYINVOICE.edit(this.alt)"+" src='imagen/edit.png' alt='"+i+"' class='btnEdit'/> </a>  <a href="+"#"+">  <img "+" onClick="+"MYINVOICE.delete(this.alt)"+" src='imagen/delete.png' alt='"+i+"' class='btnDelete'/> </a>  </td>" + 
	  								 "</tr>");
		}
	},

	delete: function(boton){
		var flag = confirm("Are you sure you want to delete this invoice?");
		if (flag) {
		index=boton;
		tbInvoices.splice(index,1);
		localStorage.setItem("invoice", JSON.stringify(tbInvoices));
		alert("Invoice deleted.");
		this.list();
		}
	},	
};

var USERS = USERS || {
	create: function() {
		if ($("#fullname").val()==""|| $("#usrname").val()==""|| $("#passwrd1").val()==""
			||$("#passwrd2").val()=="" ) {
			alert("Please dont leave any blank spaces");
		}else if($("#passwrd2").val() != $("#passwrd1").val()){
			alert("Passwords are not the same");
		}else{	
			var user={			
				fullname: document.getElementById('fullname').value,
				username: document.getElementById('usrname').value,
				password: document.getElementById('passwrd1').value,
			};

			if (localStorage.getItem('user')===null) {
				var arr_user=[user];
				localStorage.setItem('user', JSON.stringify(arr_user));
				this.list();
				CLOSE;	
			}else{		
				var str_users=localStorage.getItem('user');
				var arr_users=JSON.parse(str_users);
				arr_users.push(user);
				localStorage.setItem('user', JSON.stringify(arr_users));
				this.list();
				CLOSE;
			};
		}
	},

	list: function(){
		$("#content_table").html("");
		$("#content_table").html(
			"<thead>"+
			"	<tr id="+"tituloTablaCliente"+">"+
			"	<th>Full Name</th>"+
			"	<th>Username</th>"+
			"	<th>Password</th>"+
			"	<th>Action</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
		);
		for(var i in tbUsers){
		var user = tbUsers[i];
	  	$("#content_table tbody").append("<tr>"+
									 "	<td>"+user.fullname+"</td>" + 
									 "	<td>"+user.username+"</td>" + 
									 "	<td>"+user.password+"</td>" + 
									 "	<td>  <a href="+"#openModel_edit"+"> <img "+" onClick="+"USERS.edit(this.alt)"+" src='imagen/edit.png' alt='"+i+"' class='btnEdit'/> </a>  <a href="+"#"+">  <img "+" onClick="+"USERS.delete(this.alt)"+" src='imagen/delete.png' alt='"+i+"' class='btnDelete'/> </a>  </td>" + 
	  								 "</tr>");
		}
	},

	edit: function(boton){ 
		index=boton;
		var user = tbUsers[index];
		$("#fullname_edit").val(user.fullname);
		$("#usrname_edit").val(user.username);
		$("#passwrd1_edit").val(user.password);
		$("#passwrd2_edit").val(user.password);		
	},	
	save_edit: function(){
		if ($("#fullname_edit").val()==""|| $("#usrname_edit").val()==""|| $("#passwrd1_edit").val()==""
			||$("#passwrd2_edit").val()=="" ) {
			alert("Please dont leave any blank spaces");
		}else if($("#passwrd2_edit").val() != $("#passwrd1_edit").val()){
			alert("Passwords are not the same");
		}else {
		tbUsers[index] = {
			fullname    : $("#fullname_edit").val(),
			username  : $("#usrname_edit").val(),
			password : $("#passwrd1_edit").val()
		};
		localStorage.setItem('user', JSON.stringify(tbUsers));
		CLOSE;
		}
	},

	delete: function(boton){
		var flag = confirm("Are you sure you want to delete this user?");
		if (flag) {
		index=boton;
		tbUsers.splice(index, 1);
		localStorage.setItem("user", JSON.stringify(tbUsers));
		alert("User deleted.");
		this.list();
		CLOSE; 
		}
	},
};

function validate (){
		var username = document.getElementById('usrname').value;
		var password = document.getElementById('passwrd').value;
		if (username == "" || password == "" ) {
			alert("Please dont leave any blank spaces")
		}else {
			if (username == "admin" && password == "$uper4dmin" ) {
		 		localStorage.setItem('temp_user', 'Admin');
		 		window.open("dashboard.php?fullname=Admin","_blank");
		 		//window.location = ("dashboard.php");
		 		// window.location.href = "dashboard.php";

			 }else{
			 	
				for (var i in tbUsers) {
					if (tbUsers[i].username ==  username && tbUsers[i].password == password) {
						this.username=tbUsers[i].fullname;
						localStorage.setItem('temp_user', tbUsers[i].fullname);
						window.open("dashboard.php?fullname="+tbUsers[i].fullname,"_blank"); 
						//window.location.href = "dashboard.php";
						// $("#user_name").text(tbUsers[i].fullname);
						// $('#users_hidden').hide();
						return;
					}
				};
				alert('Invalidate user and password');
			}

		};	
};

// function user_name(){
// 	var query = window.location.search.substring(1);
//        var vars = query.split("&");
//        for (var i=0;i<vars.length;i++) {
//                var pair = vars[i].split("=");
//                if(pair[0] == 'fullname'){
//                	var temp = pair[1];
//                	temp=temp.replace("%20", " ")
//                	if (temp!="Admin") {
//                		$('#users_hidden').show();
//                	};
//                	$("#user_name").append(temp);
//                }
//        }
// };

function set_user(){
	var temp=localStorage.getItem('temp_user');
	$("#user_name").append(temp);
	if (temp!='Admin') {
		$('#users_hidden').hide();
	};
}
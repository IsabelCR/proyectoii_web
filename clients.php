<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include 'inc/header_common.php'; ?>

	<title>Clients</title>	
</head>
<body onload="set_user(), MYCLIENT.list()">
	<?php include 'inc/header.php'; ?>	

	<div class="container">
		<div class="container_title">
			<div class="pull-left" id="logo">
				<img class="img-rounded" src="imagen/clients.png">
				<!--<p class="title">Clients</p>-->
			</div>
			<a href="#openModel"><input type="button" value="New" class="btn btn-primary"></a>
		</div>

		<div class="table" >

			<table id="content_table" border="1">
				


			</table>		
		</div>
	</div>
	
	<div class="container_modal" id="openModel">
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form  id="usrform" >
				<input type="text" name="firstname" placeholder="First Name" id="firstName">
				<br/>
				<br/>
				<input type="text" name="lastname" placeholder="Lastname" id="lastName">
				<br/>
				<br/>
				<input type="text" name="id" placeholder="ID" id="id" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<input type="tel" name="phone" placeholder="Phone" id="phone" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<a href="#close">Cancel</a>
	  			<input type="submit" value="Save" class="button" onclick="MYCLIENT.create()" href="#close">
			</form>	
		</div>
	</div>

	<div class="container_modal" id="openModel_edit">
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form  id="usrform" >
				<input type="text" name="firstname" placeholder="First Name" id="fistName_edit">
				<br/>
				<br/>
				<input type="text" name="lastname" placeholder="Lastname" id="lastName_edit">
				<br/>
				<br/>
				<input type="text" name="id" placeholder="ID" id="id_edit" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<input type="tel" name="phone" placeholder="Phone" id="phone_edit" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<a href="#close">Cancel</a>
	  			<input type="submit" value="Save" class="button" onclick="MYCLIENT.save_edit()">
			</form>	
		</div>
	</div>

<?php include 'inc/footer_common.php'; ?>
</body>
</html>
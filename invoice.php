<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include 'inc/header_common.php'; ?>

	<title>Invoice</title>	
</head>
<body onload="set_user(), MYINVOICE.list()">
	<?php include 'inc/header.php'; ?>

	<div class="container">
		<div class="container_title">
			<div id="logo">
				<img src="imagen/invoices.png">
				<!--<p class="title" >Invoice</p>-->
			</div>
			<a href="#openModel"><input type="button" value="New" class="btn btn-primary" onClick="MYINVOICE.selector()"></a>
		</div>

		<div class="table">
			<table id="content_table" border="1">
				
			</table>	

		</div>
	</div>

	<div class="container_modal" id="openModel">			
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form id="usrform" >
				<select id="client_invoice">						
				</select>
				<br/>
				<br/>
				<input type="date" name="date" placeholder="Date" id="date">
				<br/>
				<br/>
				<input type="text" name="amount" placeholder="Amount" id="amount" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<a href="#close">Cancel</a>
		  		<input type="submit" value="Save" class="button" onClick="MYINVOICE.create()">
			</form>	
		</div>
	</div>

	<div class="container_modal" id="openModel_edit">
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form  id="usrform" >
				<select id="client_chambas_edit">						
				</select>
				<br/>
				<br/>
				<input type="date" name="date" placeholder="Date" id="date_edit">
				<br/>
				<br/>
				<input type="text" name="amount" placeholder="Amount" id="amount_edit" onkeypress='return event.charCode >=48 && event.charCode <=57'>
				<br/>
				<br/>
				<a href="#close">Cancel</a>
	  			<input type="submit" value="Save" class="button" onclick="MYINVOICE.save_edit()">
			</form>	
		</div>
	</div>
	

	<?php include 'inc/footer_common.php'; ?>

</body>
</html>
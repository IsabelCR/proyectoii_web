<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include 'inc/header_common.php';?>
	<title>Users</title>	
</head>
<body onload="set_user(),USERS.list()">
	
	<?php include 'inc/header.php'; ?>

	<div class="container">
		<div class="container_title">
			<div id="logo">
				<img class="image" src="imagen/user.png">
				<!--<p class="title" >Users</p>-->
			</div>
			<a href="#openModel"><input type="button" value="New" class="btn btn-primary" onClick="USERS.selector()"></a>
		</div>

		<div class="table">

			<table id="content_table" border="1">
				
			</table>	

		</div>
	</div>

	<div class="container_modal" id="openModel">			
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form id="usrform" >
				<input type="text" name="fullname" id="fullname" placeholder="Full Name">
				<br/>
				<br/>
				<input type="text" name="username" id="usrname" placeholder="username">
				<br/>
				<br/>
				<input type="password" name="date" id="passwrd1">
				<br/>
				<br/>
				<input type="password" name="date" id="passwrd2">
				<br/>
				<br/>
				<a href="#close">Cancel</a>
		  		<input type="submit" value="Save" class="button" onClick="USERS.create()">
			</form>	
		</div>
	</div>

	<div class="container_modal" id="openModel_edit">
		<div class="formClient" id="usrform">
			<a href="#close" title="close" id="close_btn" class="close" >X</a>
			<form id="usrform" >
				<input type="text" name="fullname" id="fullname_edit" placeholder="Full Name">
				<br/>
				<br/>
				<input type="text" name="username" id="usrname_edit" placeholder="username">
				<br/>
				<br/>
				<input type="password" name="date" id="passwrd1_edit">
				<br/>
				<br/>
				<input type="password" name="date" id="passwrd2_edit">
				<br/>
				<br/>
				<a href="#close">Cancel</a>
		  		<input type="submit" value="Save" class="button" onClick="USERS.save_edit()">
			</form>	
		</div>
	</div>
	

	
	<?php include 'inc/footer_common.php'; ?>
</body>
</html>